import json
from flask import make_response, abort

def escribirJson(data):
    with open('personas.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)

def cargarJson():    
    try:
        with open('personas.json') as json_file:
            data = json.load(json_file)
    except ValueError:                
        data = {}
    
    except FileNotFoundError:       
        data = {}
    
    return data

def adicionarPersona(documento, persona):    
    data = cargarJson()

    if str(documento) in data:
        return 501

    data[documento] = persona
    escribirJson(data)

    return 200

def adicionarHijo(documentoPadre, documento, hijo):
    data = cargarJson()

    if str(documentoPadre) in data:
        persona = data[str(documentoPadre)]

        if "hijos" not in persona:
            persona["hijos"] = []

        for hijito in persona["hijos"]:
            if str(documento) in hijito:
                return 501
        
        persona["hijos"].append({documento:hijo})

        escribirJson(data)

        return 200
    else:
        return 404    

def obtenerPersona(documento):
    data = cargarJson()
    
    if str(documento) in data:
        return data[str(documento)]
    else:
        return 404        

def eliminarPersona(documento):
    data = cargarJson()
    
    if str(documento) in data:
        del data[str(documento)]
        escribirJson(data)
        
        return 201
    else:
        return 404   